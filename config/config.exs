# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ashtray,
  ecto_repos: [Ashtray.Repo]

# Configures the endpoint
config :ashtray, AshtrayWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "NPNOCoPoH9v0wDkhq93GAhsDN2A3nKp01r0GhwI9zrHHbQTCj5bDd/NJs2pi8iZS",
  render_errors: [view: AshtrayWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ashtray.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Use Jason for JSON parsing in oauth2
config :oauth2,
  serializers: %{"application/json" => Jason}

config :ueberauth, Ueberauth, providers: [github: {Ueberauth.Strategy.Github, []}]

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.get_env("GITHUB_CLIENT_ID"),
  client_secret: System.get_env("GITHUB_CLIENT_SECRET")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
