defmodule Ashtray.Accounts do
  import Ecto.Query, warn: false
  alias Ashtray.Repo

  alias Ashtray.Accounts.User

  def get_user(id), do: Repo.get(User, id)

  def get_user_by(attrs), do: Repo.get_by(User, attrs)

  def register_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end
end
