defmodule Ashtray.Repo do
  use Ecto.Repo,
    otp_app: :ashtray,
    adapter: Ecto.Adapters.Postgres
end
