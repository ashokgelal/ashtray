defmodule AshtrayWeb.Auth do
  import Plug.Conn
  alias Ashtray.Accounts

  def init(opts), do: opts

  def call(conn, _opts) do
    user_id = get_session(conn, :user_id)
    user = user_id && Accounts.get_user(user_id)
    assign(conn, :current_user, user)
  end

  def login_or_register(conn, info) do
    {:ok, user} = retrieve_or_register(info)
    login(conn, user)
  end

  defp retrieve_or_register(info) do
    case Accounts.get_user_by(%{email: info.email}) do
      nil ->
        Accounts.register_user(%{
          name: info.name,
          email: info.email,
          avatar: info.image,
          handle: info.nickname
        })

      user ->
        {:ok, user}
    end
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end
end
