defmodule AshtrayWeb.AuthController do
  use AshtrayWeb, :controller
  plug Ueberauth

  alias AshtrayWeb.Auth

  def new(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    conn
    |> Auth.login_or_register(auth.info)
    |> redirect(to: Routes.page_path(conn, :index))
  end

  def delete(conn, _params) do
    conn
    |> Auth.logout()
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
