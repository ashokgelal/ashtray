defmodule AshtrayWeb.PageController do
  use AshtrayWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
