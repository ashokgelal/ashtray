defmodule AshtrayWeb.Router do
  use AshtrayWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug AshtrayWeb.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AshtrayWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/auth", AshtrayWeb do
    pipe_through :browser

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :new
    delete "/", AuthController, :delete
  end

  # Other scopes may use custom stacks.
  # scope "/api", AshtrayWeb do
  #   pipe_through :api
  # end
end
